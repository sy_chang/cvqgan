import numpy as np
from math import floor


def preprocess(X, N = 3, scaling_factor = 10.0) :
    x_sum = []
    N2 = 25//N

    for x in X:
        tmp = np.sum(x[0], axis = 0)
        x_sum.append(np.array([np.sum(tmp[N2*i:N2*(i+1)])for i in range(floor(len(tmp)/N2))]))
    X_train = np.array(x_sum)/scaling_factor

    return X_train


def create_fake_data(batch_size):
    fake_data = []
    while len(fake_data) < batch_size:
        new_fake = [np.random.uniform() for _ in range(3)]
        if new_fake[1] < new_fake[0] or new_fake[1] < new_fake[2] :
            fake_data.append(new_fake)

    return np.array(fake_data)

