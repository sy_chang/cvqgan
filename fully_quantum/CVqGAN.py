import pennylane as qml
from pennylane.templates.layers import *
import numpy as np
from pennylane.templates.embeddings import *
import h5py
from scipy.stats import entropy
from sklearn.metrics import log_loss
from pennylane.optimize import GradientDescentOptimizer, AdamOptimizer, AdagradOptimizer
from pennylane.templates.subroutines import Interferometer
import copy
import autograd

import h5py

from math import *
import math

import autograd.numpy as autonp

import matplotlib.pyplot as plt

import multiprocessing

dev = qml.device('strawberryfields.fock', wires=3, analytic=True, cutoff_dim = 10)


def BCE(actual, predicted):

    sum_score = 0.0
    for i in range(len(actual)):
        sum_score += (actual[i] * np.log(max(1e-15, predicted[i])) + \
                     (1.0 - actual[i]) * np.log(max(1e-15, 1.0 - predicted[i])))
    mean_sum_score = 1.0 / len(actual) * sum_score
    return -mean_sum_score


def sigmoid(x):

    return 1.0 / (1.0+np.exp(-40*(x-0.07)))


def reshape_parameters(depth, N, params):
    K = N*(N-1)//2
    params_reshape = []

    for i in range(4):
        params_reshape.append(np.reshape(params[i*depth*K : (i+1)*depth*K], (depth, K)))

    for i in range(7):
        params_reshape.append(np.reshape(params[depth*(4*K + i*N) : depth*(4*K + (i+1)*N)], (depth, N)))

    return params_reshape




def layer(d, N, params) :
    """CV quantum neural network layer acting on N modes

    Args:
        q (list): list of qumodes the layer is to be applied to
    """
    # begin layer
    theta1, theta2, phi1, phi2, R, phiR,   A, phiA, varphi1 , varphi2,Kerr = (i for i in range(11))

    Interferometer(params[theta1][d,:], params[phi1][d,:], params[varphi1][d,:], beamsplitter = 'clements', wires = range(N))

    for i in range(N):
        qml.Squeezing(params[R][d,i], params[phiR][d,i], wires = i)

    Interferometer(params[theta2][d,:], params[phi2][d,:],params[varphi2][d,:], beamsplitter = 'clements', wires = range(N))

    for i in range(N):
        qml.Displacement(params[A][d,i], params[phiA][d,i], wires = i)


    for i in range(N):
        #qml.Squeezing(params[Kerr][d,i],0, wires = i)
        qml.Kerr(params[Kerr][d,i], wires = i)


@qml.qnode(dev)
def generator(x, params):

    N = len(x)

    DisplacementEmbedding(x, wires = range(0, N))
    ps = params[-1].shape
    depth_g = ps[0]

    for i in range(depth_g) :
        layer(i, ps[1], params)

    GN = params[-1].shape[1]

    outputs = [qml.expval(qml.X(i)) for i in range(3)]
    return outputs

def gen_disc_circuit(x, gen_params, disc_params):
    outputs = generator(x, gen_params)
    y, memory = netD.forward(np.array([outputs]), disc_params)
    return y,  np.array(outputs)

def get_real_disc_output(inputs,  disc_params):
    output = []
    for x in inputs :
        y, memory = netD.forward([x], disc_params)
        output.append(y[0][0])

    return np.array(output)

def get_gen_disc_output(inputs,  gen_params, disc_params):
    output = []
    images = []

    for x in inputs :
        y, image = gen_disc_circuit(x, gen_params, disc_params)
        output.append(y[0][0])
        images.append(image)

    return np.array(output), np.array(images)

# Calculate cost for discriminator circuit
def cost_real_disc(inputs, labels, disc_params,  output_array, loss_array):

    output = get_real_disc_output(inputs,disc_params)
    for x in output:
        output_array.append(x)

    loss_array.append(BCE(labels, output))


# Calculate cost for combined generator-discriminator circuit
def cost_gen_disc(inputs, labels,gen_params,  disc_params, output_array, loss_array, image_array):
    output, images = get_gen_disc_output(inputs, gen_params, disc_params)
    for x in output:
        output_array.append(x)

    for x in images:
        image_array.append(x)

    loss_array.append(BCE(labels, output))



def cost_real_disc_fn(inputs, labels, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager = multiprocessing.Manager()
    output_array = manager.list()
    loss_array = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread == (n_threads - 1):
            p = multiprocessing.Process(target = cost_real_disc,
                                        args = (inputs[start:,:],labels[start:], disc_params, output_array, loss_array))
        else:
            p = multiprocessing.Process(target = cost_real_disc,
                                        args = (inputs[start:end,:], labels[start:end], disc_params,output_array, loss_array))

        jobs.append(p)
        p.start()
    for proc in jobs :
        proc.join()

    with open("real_GAN.txt", "+a") as f:
        for x in output_array:
            f.write(str(x) + " ")
        f.write("\n")

    return sum(loss_array)/len(loss_array)

def cost_gen_disc_fn(inputs, labels,gen_params,  disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager = multiprocessing.Manager()
    output_array = manager.list()
    loss_array = manager.list()
    image_array = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread == (n_threads - 1):
            p = multiprocessing.Process(target = cost_gen_disc,
                                        args = (inputs[start:,:],labels[start:],gen_params,  disc_params, output_array, loss_array, image_array, ))
        else:
            p = multiprocessing.Process(target = cost_gen_disc,
                                        args = (inputs[start:end,:], labels[start:end], gen_params, disc_params,output_array, loss_array, image_array,))

        jobs.append(p)
        p.start()
    for proc in jobs :
        proc.join()

    if labels[0] == 0 :
        with open("fake_GAN.txt", "+a") as f:
            for x in output_array:
                f.write(str(x) + " ")
            f.write("\n")
        mean_output = np.mean(np.array(image_array), axis = 0)

        with open("outputs.txt", "+a") as f:
            for x in mean_output:
                f.write(str(x) + " ")
            f.write("\n")

    return sum(loss_array)/len(loss_array)

def objective_real_disc(inputs, labels):
    def cost(x, yt, disc_params):
        output_array = get_real_disc_output(x, disc_params)
        return BCE(yt, output_array)
    return lambda v : cost(inputs, labels,  v)

def grad_real_disc(inputs, labels, disc_params, gradient_array):
    objective = objective_real_disc(inputs, labels)
    g = autograd.grad(objective)(disc_params)
    gradient_array.append(g)


def objective_gen_disc(inputs, labels, gen_params, disc_params):
    def cost(x, yt, params1,params2):
        output_array, _ = get_gen_disc_output(x, params1, params2)
        return BCE(yt, output_array)

    if labels[0] == 0:
        return lambda v : cost(inputs, labels, gen_params, v)
    else:
        return lambda v : cost(inputs, labels, v, disc_params)

def grad_gen_disc(inputs, labels, gen_params, disc_params, gradient_array):
    objective = objective_gen_disc(inputs, labels, gen_params, disc_params)
    if labels[0] == 0 :
        g = autograd.grad(objective)(disc_params)
        gradient_array.append(g)
    else :
        g = autograd.grad(objective)(gen_params)
        gradient_array.append(np.concatenate([np.reshape(x, (x.size)) for x in g]))


def grad_real_disc_fn(input, labels, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager= multiprocessing.Manager()
    gradient_array  = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread==(n_threads-1):
            p = multiprocessing.Process(target=grad_real_disc,
                                        args = (input[start:,:],labels[start:], disc_params, gradient_array,))
        else:
            p = multiprocessing.Process(target=grad_real_disc,
                                        args = (input[start:end,:],labels[start:end], disc_params, gradient_array,))

        jobs.append(p)
        p.start()
	# WAIT for jobs to finish
    for proc in jobs:
        proc.join()

    return sum(gradient_array)/len(gradient_array)

def grad_gen_disc_fn(X_input, labels,gen_params, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager= multiprocessing.Manager()
    gradient_array  = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread==(n_threads-1):
            p = multiprocessing.Process(target=grad_gen_disc,
                                        args = (input[start:,:],labels[start:], gen_params, disc_params, gradient_array,))
        else:
            p = multiprocessing.Process(target=grad_gen_disc,
                                        args = (input[start:end,:],labels[start:end], gen_params, disc_params, gradient_array,))

        jobs.append(p)
        p.start()
	# WAIT for jobs to finish
    for proc in jobs:
        proc.join()

    return sum(gradient_array)/len(gradient_array)


def get_real_fake(outputs, labels):
    real = []
    fake = []
    for x, l in zip(outputs, labels):
        if l == 0:
            fake.append(x)
        else :
            real.append(x)



def lrdecay(e, lr):
    if e < 5 :
        return 0.02
    elif 5 <= e and e < 10:
        return 0.01
    elif 10 <= e and e < 20 :
        return 0.005
    elif 20 <= e and e < 30 :
        return 0.001
    else :
        return 0.0001