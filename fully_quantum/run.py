import h5py
import numpy as np
from .CVqGAN import *
from .tools import *

if __name__ == '__main__':
    ### small data set is 10000 evts
    nevt = 10000  # number of events used for (training and testing)
    d=h5py.File("data/Electron2D_data.h5",'r')

    xd = d.get('ECAL')
    print(xd.shape)

    nx = xd.shape[2]
    ny = xd.shape[3]


    X=np.array(xd[:nevt,:,:])
    X_train = preprocess(X)
    fake_data = create_fake_data(len(X_train))


    num_epoch = 100
    batch_size = 10
    nb_batches = len(X_train)//batch_size
    n_threads = 5

    GN = 3
    DK = GN*(GN - 1)//2
    # Number of qumodes in discriminator
    DN = 3
    DK = DN*(DN-1)//2

    # Discriminator depth
    depth_d = 2
    depth_g = 1

    # Number of discriminator output
    num_params_d = depth_d*(4*DK + 7*DN)
    num_params_g = depth_g*(4*DK + 7*DN)

    disc_weights = reshape_parameters(depth_d, DN, np.array([np.random.uniform() for _ in range(num_params_d)]))
    gen_weights =  reshape_parameters(depth_g, GN, np.array([np.random.uniform() for _ in range(num_params_d)]))

    optD = qml.AdamOptimizer(0.01)
    optG = qml.AdamOptimizer(0.01)
    print("DN = ", DN, "DepthD = ", depth_d, "GN = ", GN, "DepthG = ", depth_g)

    for epoch in range(num_epoch):
        print("Epoch", epoch)
        costG = []
        costD = []

        np.random.shuffle(X_train)
        for b in range(nb_batches):

            #Real data
            X_input = X_train[b*batch_size :  (b+1)*batch_size]

            print("batch", b)
            start = time.time()
            true_labels = np.ones(batch_size)
            fake_labels = np.zeros(batch_size)
            previous = disc_weights.copy()
            disc_weights = optD.step(lambda v:cost_real_disc_fn(X_input,  true_labels, v),  disc_weights,
                                lambda z:grad_real_disc_fn(X_input,true_labels, disc_weights))
            fake_input = np.array([[np.random.normal()] for _ in range(batch_size)])

            disc_weights = optD.step(lambda v:cost_gen_disc_fn(fake_input,  fake_labels, gen_weights, v),  disc_weights,
                                lambda z:grad_gen_disc_fn(fake_input,fake_labels, gen_weights, disc_weights))
            costD.append((cost_real_disc_fn(X_input, true_labels, disc_weights) + \
                        cost_gen_disc_fn(fake_input, fake_labels, gen_weights, disc_weights))/2.0)

            fake_input = np.array([[np.random.normal()] for _ in range(batch_size)])

            previous = disc_weights.copy()

            gen_weights = optG.step(lambda v:cost_gen_disc_fn(fake_input,true_labels, v, disc_weights),  gen_weights,
                                lambda z:grad_gen_disc_fn(fake_input,true_labels, gen_weights, disc_weights))


            costG.append(cost_gen_disc_fn(fake_input,true_labels, gen_weights, disc_weights))

            print("after opt : costG = ", costG[-1] , "costD = ", costD[-1],
                    "time taken = ", (time.time() - start)/60, "min")

            tmp = np.concatenate([np.reshape(x, (x.size)) for x in disc_weights])
            with open("disc_weights.txt", "+a") as f:
                for x in tmp:
                    f.write(str(x) + " ")
                f.write("\n ")

            tmp = np.concatenate([np.reshape(x, (x.size)) for x in gen_weights])
            with open("gen_weights.txt", "+a") as f:
                for x in tmp:
                    f.write(str(x) + " ")
                f.write("\n ")
        with open("costD.txt", "+a") as f:
            f.write(str(np.mean(costD))+" ")


        with open("costG.txt", "+a") as f:
            f.write(str(np.mean(costG))+" ")
