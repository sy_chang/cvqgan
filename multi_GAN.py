import pennylane as qml
import strawberryfields as sf
from pennylane.templates.layers import *
import numpy as np
from pennylane.templates.embeddings import *
import h5py
from scipy.stats import entropy
from sklearn.metrics import log_loss
from pennylane.optimize import GradientDescentOptimizer, AdamOptimizer, AdagradOptimizer
from pennylane.templates.subroutines import Interferometer
import copy
import autograd

import h5py

from math import *
import math

import autograd.numpy as autonp

import matplotlib.pyplot as plt

import multiprocessing

import time

dev = qml.device('default.gaussian', wires=5, analytic=True)


def BCE(actual, predicted):
    sum_score = 0.0
    for i in range(len(actual)):
        sum_score += (actual[i] * autonp.log(max(1e-15, predicted[i])) + \
                     (1.0 - actual[i]) * autonp.log(max(1e-15, 1.0 - predicted[i])))
    mean_sum_score = 1.0 / len(actual) * sum_score
    return -mean_sum_score


def sigmoid(x):
    return 1.0 / (1.0+autonp.exp(-x))


def reshape_parameters(depth, N, params):
    K = N*(N-1)//2
    params_reshape = []

    for i in range(4):
        params_reshape.append(np.reshape(params[i*depth*K : (i+1)*depth*K], (depth, K)))

    for i in range(7):
        params_reshape.append(np.reshape(params[depth*(4*K + i*N) : depth*(4*K + (i+1)*N)], (depth, N)))

    return params_reshape




def layer(d, N, params) :
    """CV quantum neural network layer acting on N modes

    Args:
        q (list): list of qumodes the layer is to be applied to
    """
    # begin layer
    theta1, theta2, phi1, phi2, R, phiR,   A, phiA, varphi1 , varphi2,Kerr = (i for i in range(11))

    Interferometer(params[theta1][d,:], params[phi1][d,:], params[varphi1][d,:], beamsplitter = 'clements', wires = range(N))

    for i in range(N):
        qml.Squeezing(params[R][d,i], params[phiR][d,i], wires = i)

    Interferometer(params[theta2][d,:], params[phi2][d,:],params[varphi2][d,:], beamsplitter = 'clements', wires = range(N))

    for i in range(N):
        qml.Displacement(params[A][d,i], params[phiA][d,i], wires = i)


    for i in range(N):
        qml.Squeezing(params[Kerr][d,i],0, wires = i)
#         qml.Kerr(params[Kerr][d,i], wires = i)

def set_d_input(x):
    N = len(x)
    for i in range(N):
        qml.Squeezing(2, 0, wires = i)
#     qml.Squeezing(2, 0, wires = )
    DisplacementEmbedding(x[0:N], wires = range(0,N))


def set_g_input(x):
    qml.Squeezing(2, 0, wires = 0)
#     qml.Squeezing(2, 0, wires = )
    DisplacementEmbedding(x, wires = 0)


def discriminator(params):
    ps = params[-1].shape
    depth_d = ps[0]

    for i in range(depth_d) :
        layer(i, ps[1], params)

def generator(params):
    ps = params[-1].shape
    depth_g = ps[0]

    for i in range(depth_g) :
        layer(i, ps[1], params)

@qml.qnode(dev)
def real_disc_circuit(x, params):
    set_d_input(x)
    discriminator(params)
    N = params[-1].shape[1]
    return qml.expval(qml.X(N-1))

@qml.qnode(dev)
def gen_disc_circuit(x, gen_params, disc_params):
    set_g_input(x)
    generator(gen_params)
    discriminator(disc_params)
    GN = gen_params[-1].shape[1]
    DN = disc_params[-1].shape[1]
    return qml.expval(qml.X(DN-1))


def get_real_disc_output(inputs,  disc_params):
    output = []
    for x in inputs :
        output.append(sigmoid(real_disc_circuit(x, disc_params)))
    return np.array(output)

def get_gen_disc_output(inputs,  gen_params, disc_params):
    output = []
    for x in inputs :
        output.append(sigmoid(gen_disc_circuit(x, gen_params, disc_params)))

    return np.array(output)

def cost_real_disc(inputs, labels, disc_params,  output_array, loss_array):
    output = get_real_disc_output(inputs, disc_params)
    for x in output :
        output_array.append(x)

    loss_array.append(BCE(labels, output))


def cost_gen_disc(inputs, labels,gen_params,  disc_params, output_array, loss_array):
    output = get_gen_disc_output(inputs, gen_params, disc_params)
    for x in output:
        output_array.append(x)

    loss_array.append(BCE(labels, output))


def cost_real_disc_fn(inputs, labels, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager = multiprocessing.Manager()
    output_array = manager.list()
    loss_array = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread == (n_threads - 1):
            p = multiprocessing.Process(target = cost_real_disc,
                                        args = (inputs[start:,:],labels[start:], disc_params, output_array, loss_array))
        else:
            p = multiprocessing.Process(target = cost_real_disc,
                                        args = (inputs[start:end,:], labels[start:end], disc_params,output_array, loss_array))

        jobs.append(p)
        p.start()
    for proc in jobs :
        proc.join()

    with open("real.txt", "+a") as f:
        for x in output_array:
            f.write(str(x) + " ")
        f.write("\n")

    return sum(loss_array)/len(loss_array)

def cost_gen_disc_fn(inputs, labels,gen_params,  disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager = multiprocessing.Manager()
    output_array = manager.list()
    loss_array = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread == (n_threads - 1):
            p = multiprocessing.Process(target = cost_gen_disc,
                                        args = (inputs[start:,:],labels[start:],gen_params,  disc_params, output_array, loss_array, ))
        else:
            p = multiprocessing.Process(target = cost_gen_disc,
                                        args = (inputs[start:end,:], labels[start:end], gen_params, disc_params,output_array, loss_array,))

        jobs.append(p)
        p.start()
    for proc in jobs :
        proc.join()

    if labels[0] == 0 :
        with open("fake.txt", "+a") as f:
            for x in output_array:
                f.write(str(x) + " ")
            f.write("\n")
    return sum(loss_array)/len(loss_array)

def objective_real_disc(inputs, labels):
    def cost(x, yt, disc_params):
        output_array = get_real_disc_output(x, disc_params)
        return BCE(yt, output_array)
    return lambda v : cost(inputs, labels,  v)

def grad_real_disc(inputs, labels, disc_params, gradient_array):
    objective = objective_real_disc(inputs, labels)
    g = autograd.grad(objective)(disc_params)
    gradient_array.append(np.concatenate([np.reshape(x, (x.size)) for x in g]))


def objective_gen_disc(inputs, labels, gen_params, disc_params):
    def cost(x, yt, params1,params2):
        output_array = get_gen_disc_output(x, params1, params2)
        return BCE(yt, output_array)

    if labels[0] == 0:
        return lambda v : cost(inputs, labels, gen_params, v)
    else:
        return lambda v : cost(inputs, labels, v, disc_params)

def grad_gen_disc(inputs, labels, gen_params, disc_params, gradient_array):
    objective = objective_gen_disc(inputs, labels, gen_params, disc_params)
    if labels[0] == 0 :
        g = autograd.grad(objective)(disc_params)
    else :
        g = autograd.grad(objective)(gen_params)
    gradient_array.append(np.concatenate([np.reshape(x, (x.size)) for x in g]))


def grad_real_disc_fn(input, labels, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager= multiprocessing.Manager()
    gradient_array  = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread==(n_threads-1):
            p = multiprocessing.Process(target=grad_real_disc,
                                        args = (input[start:,:],labels[start:], disc_params, gradient_array,))
        else:
            p = multiprocessing.Process(target=grad_real_disc,
                                        args = (input[start:end,:],labels[start:end], disc_params, gradient_array,))

        jobs.append(p)
        p.start()
	# WAIT for jobs to finish
    for proc in jobs:
        proc.join()

    return sum(gradient_array)/len(gradient_array)

def grad_gen_disc_fn(input, labels,gen_params, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager= multiprocessing.Manager()
    gradient_array  = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread==(n_threads-1):
            p = multiprocessing.Process(target=grad_gen_disc,
                                        args = (input[start:,:],labels[start:], gen_params, disc_params, gradient_array,))
        else:
            p = multiprocessing.Process(target=grad_gen_disc,
                                        args = (input[start:end,:],labels[start:end], gen_params, disc_params, gradient_array,))

        jobs.append(p)
        p.start()
	# WAIT for jobs to finish
    for proc in jobs:
        proc.join()

    return sum(gradient_array)/len(gradient_array)



def preprocess(X) :
    x_sum = []

    for x in X:
        tmp = np.sum(x[0], axis = 0)
        x_sum.append(np.array([np.sum(tmp[8*i:8*(i+1)])for i in range(floor(len(tmp)/8))]))
        if np.sum(x_sum[-1]) != 0:
            x_sum[-1] /= np.sum(x_sum[-1])

    x_sum = np.array(x_sum)

    X_train = x_sum[1:200]

    return X_train

def get_real_fake(outputs, labels):
    real = []
    fake = []
    for x, l in zip(outputs, labels):
        if l == 0:
            fake.append(x)
        else :
            real.append(x)


def create_fake_data(batch_size):
    fake_data = []
    while len(fake_data) < batch_size:
        new_fake = [np.random.uniform() for _ in range(3)]
        if new_fake[1] < new_fake[0] or new_fake[1] < new_fake[2] :
            fake_data.append(new_fake)

    return np.array(fake_data)

if __name__ == '__main__':
    ### small data set is 10000 evts
    nevt = 10000  # number of events used for (training and testing)
    d=h5py.File("data/Electron2D_data.h5",'r')

    xd = d.get('ECAL')
    print(xd.shape)

    nx = xd.shape[2]
    ny = xd.shape[3]


    X=np.array(xd[:nevt,:,:])
    X_train = preprocess(X)
    fake_data = create_fake_data(len(X_train))


    num_epoch = 100
    batch_size = 10
    nb_batches = len(X_train)//batch_size
    n_threads = 5

    GN = 3
    DK = GN*(GN - 1)//2
    # Number of qumodes in discriminator
    DN = 3
    DK = DN*(DN-1)//2

    # Discriminator depth
    depth_d = 2
    depth_g = 1

    # Number of discriminator output
    num_params_d = depth_d*(4*DK + 7*DN)
    num_params_g = depth_g*(4*DK + 7*DN)

    disc_weights = reshape_parameters(depth_d, DN, np.array([np.random.uniform() for _ in range(num_params_d)]))
    gen_weights =  reshape_parameters(depth_g, GN, np.array([np.random.uniform() for _ in range(num_params_d)]))

    optD = qml.AdamOptimizer(0.01)
    optG = qml.AdamOptimizer(0.01)
    print("DN = ", DN, "DepthD = ", depth_d, "GN = ", GN, "DepthG = ", depth_g)

    for epoch in range(num_epoch):
        print("Epoch", epoch)
        costG = []
        costD = []

        np.random.shuffle(X_train)
        for b in range(nb_batches):

            #Real data
            X_input = X_train[b*batch_size :  (b+1)*batch_size]

            print("batch", b)
            start = time.time()
            true_labels = np.ones(batch_size)
            fake_labels = np.zeros(batch_size)
            previous = disc_weights.copy()
            disc_weights = optD.step(lambda v:cost_real_disc_fn(X_input,  true_labels, v),  disc_weights,
                                lambda z:grad_real_disc_fn(X_input,true_labels, disc_weights))
            fake_input = np.array([[np.random.normal()] for _ in range(batch_size)])

            disc_weights = optD.step(lambda v:cost_gen_disc_fn(fake_input,  fake_labels, gen_weights, v),  disc_weights,
                                lambda z:grad_gen_disc_fn(fake_input,fake_labels, gen_weights, disc_weights))
            costD.append((cost_real_disc_fn(X_input, true_labels, disc_weights) + \
                        cost_gen_disc_fn(fake_input, fake_labels, gen_weights, disc_weights))/2.0)

            fake_input = np.array([[np.random.normal()] for _ in range(batch_size)])

            previous = disc_weights.copy()

            gen_weights = optG.step(lambda v:cost_gen_disc_fn(fake_input,true_labels, v, disc_weights),  gen_weights,
                                lambda z:grad_gen_disc_fn(fake_input,true_labels, gen_weights, disc_weights))


            costG.append(cost_gen_disc_fn(fake_input,true_labels, gen_weights, disc_weights))

            print("after opt : costG = ", costG[-1] , "costD = ", costD[-1],
                    "time taken = ", (time.time() - start)/60, "min")

            tmp = np.concatenate([np.reshape(x, (x.size)) for x in disc_weights])
            with open("disc_weights.txt", "+a") as f:
                for x in tmp:
                    f.write(str(x) + " ")
                f.write("\n ")

            tmp = np.concatenate([np.reshape(x, (x.size)) for x in gen_weights])
            with open("gen_weights.txt", "+a") as f:
                for x in tmp:
                    f.write(str(x) + " ")
                f.write("\n ")
        with open("costD.txt", "+a") as f:
            f.write(str(np.mean(costD))+" ")


        with open("costG.txt", "+a") as f:
            f.write(str(np.mean(costG))+" ")
