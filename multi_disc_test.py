import pennylane as qml
import strawberryfields as sf
from pennylane.templates.layers import *
import numpy as np
from pennylane.templates.embeddings import *
import h5py
from scipy.stats import entropy
from sklearn.metrics import log_loss
from pennylane.optimize import GradientDescentOptimizer, AdamOptimizer, AdagradOptimizer
from pennylane.templates.subroutines import Interferometer
import copy


import h5py

from math import *
import math

import autograd.numpy as autonp

import matplotlib.pyplot as plt

import multiprocessing

import time

dev = qml.device('default.gaussian', wires=4, analytic=True)


def sigmoid(x):
    return 1.0 / (1.0+autonp.exp(-x))


def reshape_parameters(depth, N, params):
    K = N*(N-1)//2
    params_reshape = []

    for i in range(4):
        params_reshape.append(np.reshape(params[i*depth*K : (i+1)*depth*K], (depth, K)))

    for i in range(7):
        params_reshape.append(np.reshape(params[depth*(4*K + i*N) : depth*(4*K + (i+1)*N)], (depth, N)))

    return params_reshape




def layer(d, N, params) :
    """CV quantum neural network layer acting on N modes

    Args:
        q (list): list of qumodes the layer is to be applied to
    """
    # begin layer
    theta1, theta2, phi1, phi2, R, phiR,   A, phiA, varphi1 , varphi2,Kerr = (i for i in range(11))

    Interferometer(params[theta1][d,:], params[phi1][d,:], params[varphi1][d,:], beamsplitter = 'clements', wires = range(N))

    for i in range(N):
        qml.Squeezing(params[R][d,i], params[phiR][d,i], wires = i)

    Interferometer(params[theta2][d,:], params[phi2][d,:],params[varphi2][d,:], beamsplitter = 'clements', wires = range(N))

    for i in range(N):
        qml.Displacement(params[A][d,i], params[phiA][d,i], wires = i)


    for i in range(N):
        qml.Squeezing(params[Kerr][d,i],0, wires = i)
#         qml.Kerr(params[Kerr][d,i], wires = i)

def set_input(x):
    N = len(x)
    for i in range(N):
        qml.Squeezing(2, 0, wires = i)

    DisplacementEmbedding(x[0:N], wires = range(0,N))


def discriminator(params):
    ps = params[-1].shape
    depth_d = ps[0]

    for i in range(depth_d) :
        layer(i, ps[1], params)



@qml.qnode(dev)
def real_disc_circuit(x, params):
    set_input(x)
    discriminator(params)
    N = params[-1].shape[1]
    return qml.expval(qml.X(N-1))



def prob_real(disc_input, disc_params, output_array):
    for x in disc_input :
        output_array.append(sigmoid(real_disc_circuit(x, disc_params)))


def prob_fake(fake_input,  disc_params, output_array):
    for x in fake_input:
        output_array.append(sigmoid(real_disc_circuit(x, disc_params)))


def costD_fn(X_input, fake_input, disc_params,):
    real_output = costD_true(X_input, disc_params)
    fake_output = costD_fake(fake_input, disc_params)
    print("real output :", real_output)
    print("fake output : ", fake_output)
    return (-autonp.log(real_output) - autonp.log(1 - fake_output))/2



def costD_true(X_input, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager = multiprocessing.Manager()
    output_array = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread == (n_threads - 1):
            p = multiprocessing.Process(target = prob_real,
                                        args = (X_input[start:,:], disc_params, output_array,))
        else :
            p = multiprocessing.Process(target = prob_real,
                                        args = (X_input[start:end,:], disc_params,output_array,))

        jobs.append(p)
        p.start()
    for proc in jobs :
        proc.join()
    return autonp.mean(autonp.array(output_array))


def costD_fake(fake_input, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager = multiprocessing.Manager()
    output_array = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread == (n_threads - 1):
            p = multiprocessing.Process(target = prob_fake,
                                        args = (fake_input[start:,:], disc_params, output_array,))
        else :
            p = multiprocessing.Process(target = prob_fake,
                                        args = (fake_input[start:end,:], disc_params,output_array,))

        jobs.append(p)
        p.start()
    for proc in jobs :
        proc.join()
    return autonp.mean(autonp.array(output_array))



def gradD(disc_input, fake_input, disc_params, gradient_array):
    grad = np.zeros(len(np.concatenate([np.reshape(x, (x.size)) for x in disc_params])))


    for x in disc_input:
        circuit = qml.grad(real_disc_circuit, argnum=1)
        g = circuit(x, disc_params)
        grad -= np.concatenate([np.reshape(x, (x.size)) for x in g])

    for x in fake_input:
        circuit = qml.grad(real_disc_circuit, argnum=1)
        g = circuit(x, disc_params)
        grad += np.concatenate([np.reshape(x, (x.size)) for x in g])

    gradient_array.append(grad)



def gradD_fn(X_input, fake_input, disc_params):
    jobs = []
    batch_size = len(X_input)
    n_feed = batch_size//n_threads

    manager= multiprocessing.Manager()
    gradient_array  = manager.list()

    for thread in range(n_threads):
        start = thread*n_feed
        end = (thread+1)*n_feed

        if thread==(n_threads-1):
            p = multiprocessing.Process(target=gradD,
                                        args = (X_input[start:,:], fake_input[start:,:],  disc_params, gradient_array,))
        else:
            p = multiprocessing.Process(target=gradD,
                                        args = (X_input[start:end,:], fake_input[start:end,:], disc_params, gradient_array,))

        jobs.append(p)
        p.start()
	# WAIT for jobs to finish
    for proc in jobs:
        proc.join()

    return sum(gradient_array)/batch_size




def preprocess(X) :
    x_sum = []
    for x in X:
        tmp = np.sum(x[0], axis = 0)
        x_sum.append(np.array([np.sum(tmp[8*i:8*(i+1)])for i in range(floor(len(tmp)/8))]))
        if np.sum(x_sum[-1]) != 0:
            x_sum[-1] /= np.sum(x_sum[-1])



    x_sum = np.array(x_sum)

    X_train = x_sum[1:1000]

    return X_train

if __name__ == '__main__':
    ### small data set is 10000 evts
    nevt = 10000  # number of events used for (training and testing)
    d=h5py.File("data/Electron2D_data.h5",'r')

    xd = d.get('ECAL')
    print(xd.shape)

    nx = xd.shape[2]
    ny = xd.shape[3]

    X=np.array(xd[:nevt,:,:])
    X_train = preprocess(X)

    num_epoch = 100
    batch_size = 100
    nb_batches = len(X_train)//batch_size
    n_threads = 5


    # Number of qumodes in discriminator
    DN = 4
    DK = DN*(DN-1)//2

    # Discriminator depth
    depth_d = 1

    # Number of discriminator output
    num_params_d = depth_d*(4*DK + 7*DN)

    disc_weights = reshape_parameters(depth_d, DN, np.array([np.random.normal(scale = 0.1) for _ in range(num_params_d)]))


    opt = qml.GradientDescentOptimizer(0.01)

    for epoch in range(num_epoch):
        costs = []
        np.random.shuffle(X_train)
        for b in range(nb_batches):

            #Real data
            X_input = X_train[b*batch_size :  (b+1)*batch_size]

            #Random fake data
            fake_input = np.array([[np.random.uniform() for _ in range(3)] for _ in range(batch_size)])

            print("batch", b)
            start = time.time()

            disc_weights = opt.step(lambda v:costD_fn(X_input, fake_input, v), disc_weights,
                                lambda z:gradD_fn(X_input, fake_input, disc_weights))
            costs.append(costD_fn(X_input, fake_input, disc_weights))

            print("after opt : cost = ", costD_fn(X_input, fake_input, disc_weights), "time taken = ", (time.time() - start)/60, "min")

        with open("dist_test.txt", "+a") as f:
            f.write(str(np.mean(costs))+" ")
