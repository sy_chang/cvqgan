import autograd

import autograd.numpy as np
import h5py

from math import floor


class Discriminator :
    def __init__(self, architecture) :
        self.nn_architecture = architecture


    def init_layers(self, seed = 99):
        np.random.seed(seed)

        number_of_layers = len(self.nn_architecture)
        params_values = np.empty(1)

        for _, layer in enumerate(self.nn_architecture):
            activ_function_curr = layer["activation"]
            layer_input_size = layer["input_dim"]
            layer_output_size = layer["output_dim"]

            params_layer = np.random.randn(layer_output_size * (layer_input_size+1))*0.1

    #         if activ_function_curr == "leaky_relu":
    #                 params_layer = (params_layer * 2 - np.ones(np.shape(params_layer))) * 0.7
    #         elif activ_function_curr == "sigmoid":
    #             params_layer = (params_layer * 2 - np.ones(np.shape(params_layer))) * 0.2
    #         else:
    #             params_layer = params_layer * 2 - np.ones(np.shape(params_layer))
            params_values = np.append(params_values, params_layer)
            params_values.flatten()

        return params_values


    def single_layer_forward_propagation(self, A_prev, W_curr, b_curr, activation="relu"):

        def sigmoid(Z):
            return 1/(1+np.exp(-Z))

        def relu(Z):
            return np.maximum(0,Z)

        def leaky_relu(z, slope=0.2):
            return np.maximum(
                np.zeros(np.shape(z)), z) + slope * np.minimum(np.zeros(np.shape(z)), z)

        Z_curr = np.dot(W_curr, A_prev) + b_curr

        if activation is "relu":
            activation_func = relu
        elif activation is "sigmoid":
            activation_func = sigmoid
        elif activation is "leaky_relu":
            activation_func = leaky_relu
        else:
            raise Exception('Non-supported activation function')

        return activation_func(Z_curr), Z_curr


    def forward(self, X, params_values):
        memory = {}
        A_curr = X

        pointer = 0

        for idx, layer in enumerate(self.nn_architecture):
            layer_idx = idx + 1
            A_prev = A_curr

            activ_function_curr = layer["activation"]
            layer_input_size = layer["input_dim"]
            layer_output_size = layer["output_dim"]
            if idx == 0:
                A_prev = np.reshape(A_curr, (layer_input_size, len(A_curr)))
            else:
                A_prev = A_curr

            pointer_next = pointer + (layer_output_size * layer_input_size)
            W_curr = params_values[pointer:pointer_next]
            W_curr = np.reshape(W_curr, (layer_output_size, layer_input_size))
            pointer = pointer_next

            pointer_next = pointer + layer_output_size

            b_curr = np.reshape(params_values[pointer:pointer_next], (layer_output_size, 1))

            pointer = pointer_next

            A_curr, Z_curr = self.single_layer_forward_propagation(A_prev, W_curr, b_curr, activ_function_curr)

            memory["A" + str(idx)] = A_prev
            memory["Z" + str(layer_idx)] = Z_curr

        return A_curr, memory

    def set_architecture(self, architecture, seed = 99):
        self.nn_architecture = architecture
        return self.init_layers(seed)
